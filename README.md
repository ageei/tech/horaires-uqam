# Projet horaires-uqam

Le projet `horaires-uqam` est un projet étudiant visant à compiler les
informations d'horaires de cours dispensées à l'Université du Québec à
Montréal (UQAM). Le service mettra à disposition des utilisateurs un
flux de modifications apportées aux horaires afin qu'elles puisse être
traitées et manipulées librement.

Depuis la session d'automne 2018, le bot de notification de changements
aux horaires des cours du Baccalauréat en Information et Génie Logiciel
(BIGL, code 7316) n'est plus présent dans le canal #cours du Slack de
l'Association des Étudiantes et Étudiants en Informatique (AGEEI).

Le mandataire du projet cherche à rendre disponible ce service à
nouveau.

Le projet peut être fait seul ou en équipe. Pour le plaisir,
l'expérience ou la raison que vous voulez. Vous n'êtes pas obligé de
nous partager votre création et ne vous gênez pas pour demander du
support, du _feedback_ ou des précisions. Ça se veut un projet
plaisant pour garder la main durant l'été. :)

## Objectif

Le développeur devra produire une application capable de collecter
les horaires des cours de l'université et rendre disponible deux (2)
flux RSS décrivant les modifications apportées à la collection.

## Exigences

### Exigences non-fonctionnelles

#### L1 - Licence de publication

Le produit du projet doit être publié sous une licence libre, telle
qu'approuvée par l'organisation [Open Source Initiative][1].

#### U1 - Flux RSS destiné aux humains

Un des flux RSS doit faciliter la compréhension du changement par
un utilisateur humain.

#### U2 - Flux RSS destiné aux machines

Un des flux RSS doit faciliter le traitement du changement par un
utilisateur machine.

#### T1 - Déploiement via Docker

L'application doit être prête à être déployée avec Docker.

### CI

- tests unitaires
- tests d'assurance qualité
- maintenabilité: https://github.com/codeclimate/codeclimate

## Livrable

Les éléments suivants composent le produit livrable par le développeur.

- code source dans un dépôt git;
- tests permettant de vérifier le bon fonctionnement de l'application;
- documentation de conception;
- documentation d'installation;
- documentation de maintenance.

### Échéancier

Le livrable doit être remis au plus tard le 1 août 2019 afin d'évaluer
les propositions et faire l'intégration pour la rentrée.

[1]: https://opensource.org/
